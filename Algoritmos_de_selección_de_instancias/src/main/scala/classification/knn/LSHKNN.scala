package classification.knn

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.broadcast.Broadcast
import org.apache.hadoop.fs.Path
import org.apache.hadoop.fs.FSDataInputStream
import org.apache.hadoop.conf.Configuration

import java.util.logging.Level
import java.util.logging.Logger
import java.util.Random
import java.util.StringTokenizer
import java.io._
import java.util.Scanner

import scala.collection.mutable.MutableList
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.ListBuffer

import classification.abstr.TraitClassifier
import instanceSelection.lshis.ANDsTable
import utils.partitioner.RandomPartitioner
import classification.seq.knn.KNNSequential

/**
 * Approximate kNN classifier by using the LSH technique.
 * Uses the locality sensitive hashing technique for finding out approximate
 * nearest neighbours fast.
 *
 * @author Álvar Arnaiz-González
 * @version 2.0
 */
class LSHKNN extends TraitClassifier {

  /**
   * Path for logger's messages.
   */
  private val bundleName = "resources.loggerStrings.stringsLSHKNN";

  /**
   * Classifier's logger.
   */
  private val logger = Logger.getLogger(this.getClass.getName(), bundleName);

  /**
   * Number of nearest neighbours.
   */
  var k = 1

  /**
   * Conjunto de datos almacenado tras la etapa de entrenamiento.
   */
  var trainingData: RDD[LabeledPoint] = null

  /**
   * Number of AND functions.
   */
  var numAND = 10

  /**
   * Number of OR functions.
   */
  var numOR = 4

  /**
   * Buckets' size.
   */
  var width: Double = 1

  /**
   * Random seed.
   */
  var seed: Long = 1

  /**
   * Spark's context.
   */
  private var sc: SparkContext = null

  /**
   * Table with hash functions.
   */
  private var tables: ArrayBuffer[ANDsTable] = null

  /**
   * kNN calculator (sequential version).
   */
  private var knnSequential: KNNSequential = null

  override def train(trainingSet: RDD[LabeledPoint]): Unit = {
    // Assign training data.
    trainingData = trainingSet
    trainingData.persist

    sc = trainingSet.sparkContext

    tables = createANDTables(trainingSet.first().features.size, new Random(seed))

    knnSequential = new KNNSequential(k)
  }

  override def classify(instances: RDD[(Long, Vector)]): RDD[(Long, Double)] = {
    // ((test_id, test_instance), train_insts_bucket)
    var knnJoinORs: RDD[((Long, Vector), List[LabeledPoint])] = null

    instances.persist

    // For each OR
    for { i <- 0 until numOR } {
      val andTable = tables(i)

      // Map the training data set to get the bucket of each instance
      val hashTrainRDD = trainingData.map { instance =>
        (andTable.hash(instance), (0, instance))
      }

      //      println("keyTrainRDD, size: " + hashTrainRDD.count())
      //      hashTrainRDD.take(5).foreach(println)
      //      println("hashTrainRDD.countByKey: " + hashTrainRDD.countByKey())

      // Map the test data set to get the bucket of each instance
      val hashTestRDD = instances.map {
        case (indx: Long, vec: Vector) =>
          (andTable.hash(vec), (indx, vec))
      }

      //      println("keyTestRDD, size: " + hashTestRDD.count())
      //      hashTestRDD.take(5).foreach(println)

      // Join train and test RDDs
      val joinTestTrainRDD = hashTestRDD.join(hashTrainRDD).map {
        case (key, (instTest, instTrain)) => (instTest, instTrain._2)
      }

      //      println("keyUnionRDD, numKeys: " + joinTestTrainRDD.keys.count() + " / count: " + joinTestTrainRDD.count())
      //      println("DiffKeys: " + joinTestTrainRDD.countByKey())
      //      joinTestTrainRDD.take(2).foreach(println)

      // Group by key to get: test_instance, list(train_instances_same_bucket)
      val instsPerBucketGroupedRDD = joinTestTrainRDD.groupByKey.mapValues(_.toList).map {
        case (inst, list) => (inst, list.distinct)
      }

      //      println(">>>>>>>>>>>>>>>>>" + i + " instsPerBucketGroupedRDD, size: " + instsPerBucketGroupedRDD.count())
      //      instsPerBucketGroupedRDD.take(1).foreach(println)

      // Join the kNN of each OR functions
      if (i == 0) {
        knnJoinORs = instsPerBucketGroupedRDD
        knnJoinORs.persist
      } else {
        knnJoinORs.join(instsPerBucketGroupedRDD).groupByKey.mapValues(_.toList).map {
          case (inst, list) => (inst, list.distinct)
        }
      }
      //      println(i + " > knnJoinORs.countByKey: ")
      //      knnJoinORs.foreach(println)
    }

    //    println("knnJoinORs, size: " + knnJoinORs.count())
    //    knnJoinORs.take(1).foreach(println)

    // Return (instances' id, predicted classes) 
    knnJoinORs.map(knnSequential.mapClassify)
  }

  override def setParameters(args: Array[String]): Unit = {
    // Check whether or not the number of arguments is even
    if (args.size % 2 != 0) {
      logger.log(Level.SEVERE, "LSHKNNPairNumberParamError",
        this.getClass.getName)
      throw new IllegalArgumentException()
    }

    for { i <- 0 until args.size by 2 } {
      try {
        val identifier = args(i)
        val value = args(i + 1)
        assignValToParam(identifier, value)
      } catch {
        case ex: NumberFormatException =>
          logger.log(Level.SEVERE, "LSHKNNNoNumberError", args(i + 1))
          throw new IllegalArgumentException()
      }
    }

    // Error if variables has not set correctly
    if (k <= 0 || numAND <= 0 || numOR <= 0 || width <= 0) {
      logger.log(Level.SEVERE, "LSHKNNWrongArgsValuesError")
      logger.log(Level.SEVERE, "LSHKNNPossibleArgs")
      throw new IllegalArgumentException()
    }
  }

  protected def assignValToParam(identifier: String, value: String): Unit = {
    identifier match {
      case "-k"   => k = value.toInt
      case "-and" => numAND = value.toInt
      case "-w"   => width = value.toDouble
      case "-s"   => seed = value.toInt
      case "-or"  => numOR = value.toInt
      case somethingElse: Any =>
        logger.log(Level.SEVERE, "LSHKNNWrongArgsError", somethingElse.toString())
        logger.log(Level.SEVERE, "LSHKNNPossibleArgs")
        throw new IllegalArgumentException()
    }
  }

  /**
   * Computes and returns an array of AND tables.
   *
   * @param  dim  Size of the hashing functions.
   * @param  r  Random generator.
   * @return Array with the tables.
   */
  private def createANDTables(dim: Int, r: Random): ArrayBuffer[ANDsTable] = {
    var andTables: ArrayBuffer[ANDsTable] = new ArrayBuffer[ANDsTable]

    for { i <- 0 until numOR } {
      andTables += new ANDsTable(numAND, dim, width, r.nextLong)
    }

    andTables
  }

}
