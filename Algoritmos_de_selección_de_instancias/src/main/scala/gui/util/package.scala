package gui

/**
 * Aquellas clases y funciones que puedan ser de utilidad para
 * más de un componente de la interfaz.
 *
 * A diferencia del paquete [[utils]], este paquete solo contiene clases que
 * afectarán únicamente a la interfaz.
 */
package object util
