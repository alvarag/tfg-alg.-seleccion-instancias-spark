package gui

/**
 * Clases que permitan llevar labores en segundo plano
 * dentro de la interfaz sin afectar al hílo principal de ésta.
 */
package object thread
